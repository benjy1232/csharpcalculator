﻿using System;

namespace Franca
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter in a number for which operation you want to do");
            Console.WriteLine("0 to exit");
            Console.WriteLine("1 for addition");
            Console.WriteLine("2 for subtraction");
            Console.WriteLine("3 for multiplication");
            Console.WriteLine("4 for division");
            Console.WriteLine("5 for factorial");
            Console.WriteLine("6 for exponential");
            Console.WriteLine("7 for log base 10");
            Console.WriteLine("8 for natural log");
            Console.WriteLine("9 for log with base chosen by you");
            Console.WriteLine("10 for a square root");
            Console.WriteLine("11 for sin");
            Console.WriteLine("12 for cos");
            Console.WriteLine("13 for tan");
            Console.WriteLine("14 for csc");
            Console.WriteLine("15 for sec");
            Console.WriteLine("16 for cot");
            // Note to past benjy - Int64 is of type long, you didn't know this at the time
            // Also try breaking your software as quickly as possible
            double o, num1, num2;
            Double.TryParse(Console.ReadLine(), out o);
            o = Math.Round(o);
            switch (o)
            {
                case 1:
                    Console.WriteLine("Please enter in the 1st number you want to add");
                    Double.TryParse(Console.ReadLine(), out num1);
                    Console.WriteLine("Please enter in the 2nd number you want to add");
                    Double.TryParse(Console.ReadLine(), out num2);
                    Console.WriteLine("{0}+{1}={2}", num1, num2, num1 + num2);
                    break;
                case 2:
                    Console.WriteLine("Please enter in the number you want to subtract from");
                    Double.TryParse(Console.ReadLine(), out num1);
                    Console.WriteLine("Please enter in the number that you want to subtract from it");
                    Double.TryParse(Console.ReadLine(), out num2);
                    Console.WriteLine("{0}-{1}={2}", num1, num2, num1 - num2);
                    break;
                case 3:
                    Console.WriteLine("Please enter in the number that you want to multiply");
                    Double.TryParse(Console.ReadLine(), out num1);
                    Console.WriteLine("Please enter in the second number you want to multiply by");
                    Double.TryParse(Console.ReadLine(), out num2);
                    Console.WriteLine("{0}*{1}={2}", num1, num2, num1 * num2);
                    break;
                case 4:
                    Console.WriteLine("Please enter in the number that you want to divide");
                    double.TryParse(Console.ReadLine(), out num1);
                    Console.WriteLine("Please enter in the number that you are dividing by");
                    double.TryParse(Console.ReadLine(), out num2);
                    Console.WriteLine("{0}/{1}={2}", num1, num2, num1 / num2);
                    break;
                case 5:
                    Console.WriteLine("Please enter in the number you want to solve the factorial for");
                    long factorial;
                    long.TryParse(Console.ReadLine(), out factorial);
                    double x = 1;
                    double y = 1;
                    do
                    {
                        y = x * y;
                        x++;
                    } while (x <= factorial);
                    Console.WriteLine("{0}!={1}", factorial, y);
                    break;
                case 6:
                    Console.WriteLine("Please enter in the number that you want to raise to a power");
                    double.TryParse(Console.ReadLine(), out num1);
                    Console.WriteLine("Plese enter in the number that you are raising it to");
                    double.TryParse(Console.ReadLine(), out num2);
                    Console.WriteLine("{0}^{1}={2}", num1, num2, Math.Pow(num1, num2));
                    break;
                case 7:
                    Console.WriteLine("Please enter in the nmber whos common log you want to find");
                    Double.TryParse(Console.ReadLine(), out num1);
                    Console.WriteLine("log({0})={1}", num1, Math.Log10(num1));
                    break;
                case 8:
                    Console.WriteLine("Please enter in the number whose natural log you want to find");
                    Double.TryParse(Console.ReadLine(), out num1);
                    Console.WriteLine("ln({0})={1}", num1, Math.Log(num1));
                    break;
                case 9:
                    Console.WriteLine("Please enter in the log base");
                    Double.TryParse(Console.ReadLine(), out num1);
                    Console.WriteLine("Please enter in the number that isn't the log base");
                    Double.TryParse(Console.ReadLine(), out num2);
                    while (num2! > 0)
                    {
                        Console.WriteLine("Unable to find log of a negative number");
                        break;
                    }
                    Console.WriteLine("Log base {0} of {1} is {2}", num1, num2, Math.Log(num2, num1));
                    break;
                case 10:
                    Console.WriteLine("Please enter in the number whose square root you want to find");
                    Double.TryParse(Console.ReadLine(), out num1);
                    Console.WriteLine("The square root of {0} is {1}", num1, Math.Sqrt(num1));
                    break;
                case 11:
                    Console.WriteLine("Please enter in degrees the sin of the angle you want to find");
                    Double.TryParse(Console.ReadLine(), out num1);
                    Console.WriteLine("sin({0})={1}", num1, Math.Sin(num1));
                    break;
                case 12:
                    Console.WriteLine("Please enter the angle in radians");
                    Double.TryParse(Console.ReadLine(), out num1);
                    Console.WriteLine("cos({0})={1}", num1, Math.Cos(num1));
                    break;
                case 13:
                    Console.WriteLine("Please enter the angle in radians");
                    Double.TryParse(Console.ReadLine(), out num1);
                    Console.WriteLine("tan({0})={1}", num1, Math.Tan(num1));
                    break;
                case 14:
                    Console.WriteLine("Please enter the angle in radians");
                    Double.TryParse(Console.ReadLine(), out num1);
                    Console.WriteLine("csc({0})={1}", num1, 1 / Math.Sin(num1));
                    break;
                case 15:
                    Console.WriteLine("Please enter the angle in radians");
                    Double.TryParse(Console.ReadLine(), out num1);
                    Console.WriteLine("sec({0})={1}", num1, 1 / Math.Cos(num1));
                    break;
                case 16:
                    Console.WriteLine("PLese enter the angle in radians");
                    Double.TryParse(Console.ReadLine(), out num1);
                    Console.WriteLine("cot({0})={1}", num1, 1 / Math.Tan(num1));
                    break;
                case 0:
                    return;
            }
        }
    }
}
